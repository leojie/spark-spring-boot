package com.dr.leo.sparkspring.config;

import org.apache.spark.sql.SparkSession;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author leo.jie (weixiao.me@aliyun.com)
 * @version 1.0
 * @organization DataReal
 * @website https://www.jlpyyf.com
 * @date 2019-08-05 21:36
 * @since 1.0
 */
@Configuration
public class SparkContextBean {
    @Bean
    @ConditionalOnMissingBean(SparkSession.class)
    public SparkSession sparkSession() throws Exception {
        return SparkSession.builder()
                .appName("SparkSpringBootApp")
                //.master("local[4]")
                .enableHiveSupport()
                .getOrCreate();
    }
}
