package com.dr.leo.sparkspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author leo.jie (weixiao.me@aliyun.com)
 * @version 1.0
 * @organization DataReal
 * @website https://www.jlpyyf.com
 * @date 2019-08-05 21:29
 * @since 1.0
 */
@SpringBootApplication
public class SparkSpringBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(SparkSpringBootApplication.class, args);
    }

}
