package com.dr.leo.sparkspring.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.dr.leo.sparkspring.service.HiveService;
import com.dr.leo.sparkspring.service.WordCountService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author leo.jie (weixiao.me@aliyun.com)
 * @version 1.0
 * @organization DataReal
 * @website https://www.jlpyyf.com
 * @date 2019-08-05 22:01
 * @since 1.0
 */
@RestController
@RequestMapping("/action")
public class ActionController {
    private final HiveService hiveService;
    private final WordCountService wordCountService;

    public ActionController(HiveService hiveService, WordCountService wordCountService) {
        this.hiveService = hiveService;
        this.wordCountService = wordCountService;
    }

    @GetMapping("/showHiveTables")
    public JSONArray showHiveTables() {
        String hiveTables = hiveService.hiveTables();
        return JSON.parseArray(hiveTables);
    }

    @GetMapping("/wordCount")
    public JSONArray wordCount(@RequestParam String filePath) {
        return wordCountService.wordCount(filePath);
    }
}
