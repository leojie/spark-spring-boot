package com.dr.leo.sparkspring.service


import com.alibaba.fastjson.JSONArray
import com.dr.leo.sparkspring.util.ObjGenerator
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.catalog.ExternalCatalog
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
  * @author leo.jie (weixiao.me@aliyun.com)
  * @organization DataReal
  * @version 1.0
  * @website https://www.jlpyyf.com
  * @date 2019-08-05 21:52
  * @since 1.0
  */
@Service
class HiveService {
  @Autowired
  private val sparkSession: SparkSession = null

  def hiveTables(): String = {
    val tableArray = new JSONArray()
    val externalCatalog = getHiveCatalog(sparkSession)
    externalCatalog.listDatabases("*")
      .foreach(db=>{
        externalCatalog.listTables(db)
          .foreach(tb=>{
            tableArray.add(ObjGenerator.newJSON(Seq(("type", "hive"), ("db", db), ("table", tb)): _*))
          })
      })
    tableArray.toJSONString
  }

  //不同版本的spark获取hive元数据信息的方式可能不一样
  private def getHiveCatalog(sparkSession: SparkSession): ExternalCatalog = {
    sparkSession.sharedState.externalCatalog.asInstanceOf[ExternalCatalog]
  }
}
