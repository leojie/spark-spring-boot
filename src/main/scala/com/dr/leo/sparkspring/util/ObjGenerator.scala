package com.dr.leo.sparkspring.util

import com.alibaba.fastjson.JSONObject

/**
  * @author leo.jie (weixiao.me@aliyun.com)
  * @organization DataReal
  * @version 1.0
  * @website https://www.jlpyyf.com
  * @date 2019-08-05 21:42
  * @since 1.0
  */
object ObjGenerator {
  def newJSON(tuples: (String, Any)*): JSONObject = {
    tuples.foldLeft(new JSONObject()) {
      case (obj, (k, v)) => obj.put(k, v)
        obj
    }
  }
}
